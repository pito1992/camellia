$(function() {
	$("a[role='button']").on('click', function(e) {
		event.preventDefault();
	});
	
	function centerModal() {
	    $(this).css('display', 'block');
	    var $dialog = $(this).find(".modal-dialog");
	    var offset = ($(window).height() - $dialog.height()) / 2;
	    // Center modal vertically in window
	    $dialog.css("margin-top", offset);
	}
	$('.modal').on('show.bs.modal', centerModal);
	$(window).on("resize", function () {
	    $('.modal:visible').each(centerModal);
	});

	$(".item.active").find(".carousel-caption").addClass('active');

	$('#top_Banner').on('slide.bs.carousel', function () {
		$(".item").find(".carousel-caption").removeClass('active');
	})
	
	$('#top_Banner').on('slid.bs.carousel', function () {
		setTimeout(function() {
			$(".item.active").find(".carousel-caption").addClass('active');
		}, 500);
	})
	$('#top_Banner').carousel({
		interval: 3000
	});
});